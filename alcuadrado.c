#include <stdio.h>

int main(void){
    int num;					// Declarar variable num.
    int res;					// Declarar variable res.

    printf("%s", "Ingrese un n�mero entero: ");	// Preguntar al usuario un n�mero entero.
    scanf("%d", &num);				// Obtener del usuario un n�mero entero y asignar valor a num.

    res = num * num;				// Calcular la potencia 2 de num y guardar en res.

    printf("El cuadrado de %d es %d.\n", num, res); // Imprimir en pantalla num y res.

    return 0;
}
