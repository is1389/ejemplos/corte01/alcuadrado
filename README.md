# Al Cuadrado

Escribe un programa que pregunte un número entero al usuario, lo obtenga y
retorne el número al cuadrado.

## Refinamiento 1

1. Preguntar al usuario un número entero.
1. Calcular la potencia 2 del número.
1. Imprimir en pantalla el resultado.

## Refinamiento 2

1. Declarar variable `num`.
1. Declarar variable `res`.
1. Preguntar al usuario un número entero.
1. Obtener del usuario un número entero y asignar valor a `num`.
1. Calcular la potencia 2 de `num` y guardar en `res`.
1. Imprimir en pantalla `num` y `res`.
